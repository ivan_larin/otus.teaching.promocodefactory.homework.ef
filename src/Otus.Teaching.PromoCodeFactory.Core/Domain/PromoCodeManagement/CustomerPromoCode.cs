﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPromoCode : BaseEntity
    {
        public virtual PromoCode PromoCode { get; set; }

        public Guid PromoCodeId { get; set; }

        public virtual Customer Customer { get; set; }

        public Guid CustomerId { get; set; }
    }
}
