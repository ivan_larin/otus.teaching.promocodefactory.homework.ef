﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<CustomerPromoCode> _customerPromoCodeRepository;

        public PromoCodesController(
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Customer> customerRepository, 
            IRepository<CustomerPromoCode> customerPromoCodeRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _customerRepository = customerRepository;
            _customerPromoCodeRepository = customerPromoCodeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();

            var response = promoCodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                BeginDate = x.BeginDate.ToLongDateString(),
                EndDate = x.EndDate.ToLongDateString(),
                ServiceInfo = x.ServiceInfo,
                Code = x.Code,
                PartnerName = x.PartnerName
            });

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var promoCode = new PromoCode()
            {
                Id = Guid.NewGuid(),
                PreferenceId = Guid.Parse(request.PreferenceId),
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                PartnerManagerId = Guid.Parse(request.PartnerManagerId),
                ServiceInfo = request.ServiceInfo
            };

            await _promoCodesRepository.AddAsync(promoCode);
            
            var customers = await _customerRepository.WhereAsync(x =>
                x.CustomerPreferences.Any(item => item.PreferenceId == Guid.Parse(request.PreferenceId)));

            foreach (var customer in customers)
            {
                await _customerPromoCodeRepository.AddAsync(new CustomerPromoCode()
                {
                    Id = Guid.NewGuid(),
                    CustomerId = customer.Id,
                    PromoCodeId = promoCode.Id
                });
            }

            return Ok();
        }
    }
}