﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _customerRepository = customerRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }

        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = customers.Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
            return Ok(response);
        }

        /// <summary>
        /// Получение клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();
            var response = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.CustomerPreferences.Select(x => new PreferenceResponse()
                {
                    Id = x.Preference.Id,
                    Name = x.Preference.Name
                }).ToList(),
                PromoCodes = customer.CustomerPromoCodes.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.PromoCode.Id,
                    Code = x.PromoCode.Code,
                    ServiceInfo = x.PromoCode.ServiceInfo,
                    BeginDate = x.PromoCode.BeginDate.ToLongDateString(),
                    EndDate = x.PromoCode.EndDate.ToLongDateString(),
                    PartnerName = x.PromoCode.PartnerName
                }).ToList()
            };
            return Ok(response);
        }

        /// <summary>
        /// Cоздание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customerId = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = customerId,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                CustomerPreferences = request.PreferenceIds.Select(prefId => new CustomerPreference()
                {
                    Id = Guid.NewGuid(),
                    PreferenceId = prefId,
                    CustomerId = customerId
                }).ToList()
            };
            await _customerRepository.AddAsync(customer);
            return Ok();
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            foreach (var item in customer.CustomerPreferences.ToList())
            {
                await _customerPreferenceRepository.DeleteAsync(item);
            }

            var preferencesToAdd = request.PreferenceIds.Select(prefId => new CustomerPreference()
            {
                Id = Guid.NewGuid(),
                PreferenceId = prefId,
                CustomerId = customer.Id
            }).ToList();

            foreach (var customerPreference in preferencesToAdd)
            {
                await _customerPreferenceRepository.AddAsync(customerPreference);
            }

            await _customerRepository.UpdateAsync(customer);
            return Ok();
        }

        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}