﻿using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public DbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Initialize()
        {
            _dataContext.Database.EnsureCreated();

            var role = _dataContext.Roles.FirstOrDefault();
            if (role == null)
            {
                _dataContext.AddRange(FakeDataFactory.Roles);
                _dataContext.SaveChanges();
                _dataContext.AddRange(FakeDataFactory.Employees);
                _dataContext.SaveChanges();
                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();
                _dataContext.AddRange(FakeDataFactory.PromoCodes);
                _dataContext.SaveChanges();
                _dataContext.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();
            }
        }
    }
}