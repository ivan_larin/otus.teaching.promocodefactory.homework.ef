﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                AppliedPromoCodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                AppliedPromoCodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = Guid.Parse("08810ba9-207c-403b-b2eb-de1cc2a83b7a"),
                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1),
                Code = "TestCode",
                PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PartnerName = "ООО Газмяс",
                ServiceInfo = "TestInfo"
            },
            new PromoCode()
            {
                Id = Guid.Parse("071067a8-d8f2-4a41-84f7-d09276bdf139"),
                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1),
                Code = "TestCode2",
                PartnerManagerId = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                PartnerName = "ООО Рога и копыта",
                ServiceInfo = "TestInfo2"
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        CustomerPreferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference()
                            {
                                Id = Guid.Parse("9f0eec30-8b33-43a2-abcf-207718d01783"),
                                CustomerId = customerId,
                                PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd")
                            },
                            new CustomerPreference()
                            {
                                Id = Guid.Parse("922f8e96-d05f-4e83-9906-19e4f97a26b4"),
                                CustomerId = customerId,
                                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                            }
                        },
                        CustomerPromoCodes = new List<CustomerPromoCode>()
                        {
                            new CustomerPromoCode()
                            {
                                Id = Guid.Parse("26547390-af86-4e3e-806b-b9214cf0c459"),
                                CustomerId = customerId,
                                PromoCodeId = Guid.Parse("08810ba9-207c-403b-b2eb-de1cc2a83b7a")
                            },
                            new CustomerPromoCode()
                            {
                                Id = Guid.Parse("017bd9ee-1215-432a-b801-6b1d1773538e"),
                                CustomerId = customerId,
                                PromoCodeId = Guid.Parse("071067a8-d8f2-4a41-84f7-d09276bdf139")
                            }
                        }
                    }
                };

                return customers;
            }
        }
    }
}