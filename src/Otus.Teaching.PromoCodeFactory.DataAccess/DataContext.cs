﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Role> Roles { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<CustomerPreference> CustomerPreference { get; set; }

        public DbSet<CustomerPromoCode> CustomerPromoCode { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>()
                .Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired();

            modelBuilder.Entity<Employee>()
                .Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<Employee>()
                .Property(x => x.FirstName)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<Employee>()
                .Property(x => x.LastName)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<Employee>()
                .HasOne(x => x.Role)
                .WithMany()
                .HasForeignKey(x => x.RoleId)
                .IsRequired();

            modelBuilder.Entity<Customer>()
                .Property(x => x.Email)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<Customer>()
                .Property(x => x.FirstName)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<Customer>()
                .Property(x => x.LastName)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<Customer>()
                .HasMany(x => x.CustomerPreferences)
                .WithOne(x => x.Customer)
                .HasForeignKey(x => x.CustomerId);
            modelBuilder.Entity<Customer>()
                .HasMany(x => x.CustomerPromoCodes)
                .WithOne(x => x.Customer)
                .HasForeignKey(x => x.CustomerId);

            modelBuilder.Entity<Preference>()
                .Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<Preference>()
                .HasMany(x => x.CustomerPreferences)
                .WithOne(x => x.Preference)
                .HasForeignKey(x => x.PreferenceId);

            modelBuilder.Entity<PromoCode>()
                .Property(x => x.Code)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<PromoCode>()
                .Property(x => x.ServiceInfo)
                .HasMaxLength(2048)
                .IsRequired();
            modelBuilder.Entity<PromoCode>()
                .Property(x => x.PartnerName)
                .IsRequired()
                .HasMaxLength(255);
            modelBuilder.Entity<PromoCode>()
                .HasOne(x => x.PartnerManager)
                .WithMany()
                .HasForeignKey(x => x.PartnerManagerId)
                .IsRequired();
            modelBuilder.Entity<PromoCode>()
                .HasOne(x => x.Preference)
                .WithMany()
                .HasForeignKey(x => x.PreferenceId)
                .IsRequired();
            modelBuilder.Entity<PromoCode>()
                .HasMany(x => x.CustomerPromoCodes)
                .WithOne(x => x.PromoCode)
                .HasForeignKey(x => x.PromoCodeId)
                .IsRequired();
        }
    }
}
